<?php
	function chlc_theme_preprocess_page(&$variables)
	{
		$page = $variables["page"];
		$variables["chlc_theme"] = "threeFourths";
		$variables["chlc_theme_imgs"] = "imgOneThird";
		
		if(empty($page["right_column"]))
		{
			//need to assign a class "fullWidth" to main content
			$variables["chlc_theme"] = "fullWidth";
		}
		
		if(!empty($page["image_2"]) && !empty($page["image_3"]))
		{
			$variables["chlc_theme_imgs"] = "imgOneThird";
		}
		elseif(!empty($page["image_2"]) || !empty($page["image_3"]))
		{
			$variables["chlc_theme_imgs"] = "imgOneHalf";
		}
		else
		{
			$variables["chlc_theme_imgs"] = "fullWidth";
		}
	}
	
	function chlc_theme_preprocess_html(&$variables) {
		drupal_add_css('http://fonts.googleapis.com/css?family=Cabin+Condensed', array('type' => 'external'));
	}
?>