<div class="siteContainer shadowAllSides">
	<div class="container">
		<!-- header -->
		<div class="bannerImg">
			<?php print render($page['banner']); ?>
		</div> <!-- end header -->
		
		<!-- nav -->
		<div class="nav">
			<?php 
				print theme('links',array('links'=>$main_menu));
			?>
		</div> <!-- end nav -->
		
		<!-- content -->
		<div class="mainContent">
			<!-- main content -->
			<div class="<?php print $variables["chlc_theme"]; ?> floatLeft">
				<!-- images -->
				<div class="<?php print $variables["chlc_theme_imgs"]; ?> floatLeft">
					<?php print render($page['image_1']); ?>
				</div>
				
				<div class="<?php print $variables["chlc_theme_imgs"]; ?> floatLeft">
					<?php print render($page['image_2']); ?>
				</div>
				
				<div class="<?php print $variables["chlc_theme_imgs"]; ?> floatLeft">
					<?php print render($page['image_3']); ?>
				</div>
				
				<div class="clearBoth"></div>
				<!-- end images -->
				
				<?php print render($page['content']); ?>
			</div> <!-- end main content -->
			
			<!-- right column -->
            <?php if($page['right_column']): ?>
				<div class="oneFourth floatLeft">
					<?php print render($page['right_column']); ?>
            	</div>
            <?php endif; ?> <!-- end right column -->
		</div> <!-- end content -->
		
		<div class="clearBoth"></div>
		
		<!-- footer -->
		<div class="footer">
			<p>Address: 511 Des Moines St. | Phone: 515-244-8913 | E-Mail: info@chlcdesmoines.org</p>
		</div> <!-- end footer -->
		
		<div class="clearBoth"></div>

	</div> <!-- end container -->
</div> <!-- end siteContainer -->